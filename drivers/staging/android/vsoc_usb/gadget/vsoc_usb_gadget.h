/*
 * Part of Android VSoC USB Gadget Controller Driver.
 *
 * Copyright (C) 2017 Google, Inc.
 *
 * Author: romitd@google.com
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Based on drivers/usb/gadget/udc/dummy_hcd.c - Dummy/Loopback USB Host and
 * device emulator driver.
 *  Copyright (C) 2003 David Brownell
 *  Copyright (C) 2003-2005 Alan Stern
 */

#ifndef __VSOC_USB_GADGET_H
#define __VSOC_USB_GADGET_H

#include "vsoc_usb_common.h"
#include "vsoc_usb_regs.h"
#include "vsoc_usb_shm.h"

#include <linux/kthread.h>
#include <linux/freezer.h>
#include <linux/interrupt.h>
#include <linux/usb/gadget.h>
#include <linux/wait.h>

#define	VSOC_USB_FIFO_SIZE   64

#define Dev_Request	(USB_TYPE_STANDARD | USB_RECIP_DEVICE)
#define Dev_InRequest	(Dev_Request | USB_DIR_IN)
#define Intf_Request	(USB_TYPE_STANDARD | USB_RECIP_INTERFACE)
#define Intf_InRequest	(Intf_Request | USB_DIR_IN)
#define Ep_Request	(USB_TYPE_STANDARD | USB_RECIP_ENDPOINT)
#define Ep_InRequest	(Ep_Request | USB_DIR_IN)


struct vsoc_usb_gadget_ep {
	struct list_head queue;
	unsigned long last_io;
	struct usb_gadget *gadget;
	const struct usb_endpoint_descriptor *desc;
	struct usb_ep ep;
	unsigned long transaction_state;
	unsigned halted:1;
	unsigned wedged:1;
	unsigned nak:1;
	u8 nak_direction;
};

/*
 * State of a USB request.
 */
enum vsoc_gadget_request_state {
	INIT_STATE = 0x0,
	CONTROL_SETUP_WAIT_STATE,
	CONTROL_IN_WAIT_STATE,
	CONTROL_IN_ACK_STATE,
	CONTROL_OUT_WAIT_STATE,
	CONTROL_STATUS_WAIT_STATE,
	DATA_IN_WAIT_STATE,
	DATA_IN_ACK_STATE,
	DATA_OUT_STATE,
};

struct vsoc_usb_gadget_request {
	struct list_head queue;
	struct usb_request req;
	unsigned long request_state;
};

/*
 * The top bit in the transaction state will be set if the URB is in flight.
 */
#define REQUEST_IN_FLIGHT_BIT \
	(sizeof(((struct vsoc_usb_gadget_request *)(0))->request_state) * 8 - 1)


struct vsoc_usb_gadget {
	spinlock_t gadget_lock;
	struct vsoc_usb_shm *shm;
	struct task_struct *tx_thread, *rx_thread;
	struct vsoc_usb_gadget_ep *gep;
	struct usb_gadget_driver *driver;
	wait_queue_head_t txq, rxq;
	struct tasklet_struct gadget_tasklet;
	unsigned long controller_action;
	unsigned long rx_action;
	unsigned long tx_action;
	unsigned long rx_action_reason[VSOC_NUM_ENDPOINTS];
	unsigned long tx_action_reason[VSOC_NUM_ENDPOINTS];
	int address; /* usb device address */
	struct usb_gadget gadget;
	struct vsoc_usb_gadget_request fifo_req;
	u8 fifo_buf[VSOC_USB_FIFO_SIZE];
	u16 devstatus;
	unsigned udc_suspended:1;
	unsigned pullup:1;
};

extern const char gadget_name[];
extern const char ep0name[];

const char *vsoc_usb_gadget_get_ep_name(int i);
const struct usb_ep_caps *vsoc_usb_gadget_get_ep_caps(int i);

#endif /* __VSOC_USB_GADGET_H */
